import multiprocessing

bind = "0.0.0.0:80"
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = "gevent"
threads = 2 * multiprocessing.cpu_count()
timeout = 6000
accesslog = "logs/access.log"
errorlog = "logs/error.log"
loglevel = "info"
pidfile = "logs/process_id.pid"
capture_output = True
enable_stdio_inheritance = True
daemon = True

#
# This is pretty important for us.  We want to run a background thread in the
# gunicorn master process (and run necessary db migrations once), and that is
# kinda hard to do.  We could force the server to run the importer app once
# via the on_starting hook below, but that probably confuses things somewhere.
# May as well just be straightforward and preload... we don't care about the
# per-worker reload semantics for this (containerized) app.
#
preload_app = True

#def on_starting(server):
    #server.log.info("master worker pid %d",os.getpid())
    #server.app.wsgi()
