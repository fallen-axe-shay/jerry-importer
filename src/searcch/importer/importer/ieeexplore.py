
import sys
import six
import logging
import time
import dateutil.parser
import datetime
import requests
from future.utils import raise_from
from urllib.parse import urlparse
import rispy
import json

from searcch.importer.importer import BaseImporter
from searcch.importer.db.model import (
    Artifact,ArtifactFile,ArtifactMetadata,ArtifactRelease,User,Person,
    Importer,Affiliation,ArtifactAffiliation,ArtifactTag,Venue,ArtifactVenue)

LOG = logging.getLogger(__name__)

class IeeeXploreImporter(BaseImporter):
    """Provides an IeeeXplore DOI Importer."""

    name = "ieeexplore"
    version = "0.1"

    @classmethod
    def _extract_record_id(self,url):
        try:
            urlobj = urlparse(url)
            if urlobj.netloc == "doi.ieeecomputersociety.org":
                doi = urlobj.path[1:].rstrip("/")
                url = "https://doi.org/" + doi
            session = requests.session()
            res = requests.get(url)
            urlobj = urlparse(res.url)
            if urlobj.netloc == "ieeexplore.ieee.org" \
              and urlobj.path.startswith("/document/"):
                return (urlobj.path[len("/document/"):].rstrip("/"),res.url,session)
        except BaseException:
            return None

    @classmethod
    def can_import(cls,url):
        """Checks to see if this URL is a doi.org or ieeexplore.ieee.org/document URL."""
        if cls._extract_record_id(url):
            return True
        return False

    def import_artifact(self,candidate):
        """Imports an artifact from IEEE Xplore and returns an Artifact, or throws an error."""
        url = candidate.url
        LOG.debug("importing '%s' from IEEE Xplore" % (url,))
        (record_id,newurl,session) = self.__class__._extract_record_id(url)
        res = session.get(
            "https://ieeexplore.ieee.org/rest/search/citation/format?recordIds=%s&download-format=download-ris" % (record_id),
            headers={"Referer":newurl})
        res.raise_for_status()
        LOG.debug("record: %r",res.json())
        j = rispy.loads(res.json()["data"])[0]
        title = name = j.get("title")
        if not title:
            raise Exception("no title in RIS metadata")
        doi = j.get("doi","")
        if not doi:
            raise Exception("no DOI in RIS metadata")
        description = j.get("abstract")
        if not description:
            LOG.warning("%s (%s) missing abstract",newurl,url)

        authors = []
        for a in j.get("authors",[]):
            authors.append(Person(name=a))
        affiliations = []
        for person in authors:
            LOG.debug("adding author %r",person)
            affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=person),roles="Author"))

        tags = list()
        seentags = dict()
        for kw in j.get("keywords",[]):
            if kw in seentags:
                continue
            tags.append(ArtifactTag(tag=kw,source="ieeexplore"))
            seentags[kw] = True

        metadata = list()
        verbatim_metadata_names = [
            "doi","start_page","end_page","secondary_title","type_of_reference",
            "year","journal_name","number","issn","publication_year" ]
        for vmn in verbatim_metadata_names:
            if not vmn in j:
                continue
            metadata.append(ArtifactMetadata(name=vmn,value=str(j[vmn])))

        record_url = "https://doi.org/%s" % (doi,)

        files = []
        LOG.debug("attempting PDF fetch via %r from IEEE Xplore",url)
        try:
            res = session.get(url)
            res.raise_for_status()
            href = "https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=" + record_id
            res = session.head(href)
            if res.status_code == requests.codes.ok:
                filename = record_id + ".pdf"
                files.append(
                    ArtifactFile(url=href,name=filename,filetype="application/pdf"))
                LOG.debug("found PDF file at %r",href)
            else:
                LOG.warning("failed to HEAD the pdf link (%r,%r); cannot download",
                            href,res.status_code)
        except:
            LOG.warning("failed PDF fetch via %r from IEEE Xplore",url)
        # Extracting the Venue Information from IEEE.
        try:
            venue_type="other"
            title = None
            venue_url = url
            verified = False
            if "type_of_reference" in j: 
                if j["type_of_reference"]  == "CONF":
                    venue_type  = "conference"
                elif j["type_of_reference"]  == "JOUR":
                    venue_type = "journal"
                elif j["type_of_reference"] == "MAG":
                        venue_type = "magazine"
            artifact_venue = []
            if "journal_name" in j:
                title = j["journal_name"]
            venue_object = self.session.query(Venue).\
                filter(Venue.title  == title).\
                filter(Venue.verified == True).first()
            if venue_object:
                artifact_venue.append(ArtifactVenue(venue=venue_object))
                verified = True
            keys = ['title' , 'abbrev' , 'url' , 'description' , 'location' , 'year' , 'month' , 'start_day' ,'end_day' , 'publisher' , 'publisher_location' , 'publisher_url' , 'isbn' , 'issn' , 'doi' , 'volume' , 'issue' , 'verified']
            value = dict.fromkeys(keys)
            value['type']=venue_type
            value['title']=title
            value['url']=url
            value['verified']=verified
            if "isbn" in j:
                value["isbn"] = j["isbn"]
            if "issn" in j:
                value["issn"] = j["issn"]
            if "volume" in j:
                value["volume"] = j["volume"]
            if "doi" in j:
                value["doi"] = j["doi"]
            if len(artifact_venue) == 0:
                artifact_venue.append(ArtifactVenue(venue = Venue(title = value['title'] , type = value['type'],abbrev = value['abbrev'], url = value['url'], description = value['description'], location = value['location'], year = value['year'], month = value['month'], start_day = value['start_day'],end_day = value['end_day'],publisher = value['publisher'], publisher_location = value['publisher_location'], publisher_url = value['publisher_url'], isbn = value['isbn'], issn = value['issn'],doi = value['doi'], volume = value['volume'],issue = value['issue'], verified = value['verified'])))
        except:
            LOG.warn("failed to scrape existing venue info")
            LOG.exception(sys.exc_info()[1])
        return Artifact(
            type="publication",url=record_url,title=title,description=description,
            name=name,ctime=datetime.datetime.now(),ext_id=record_id,
            owner=self.owner_object,importer=self.importer_object,
            tags=tags,meta=metadata,affiliations=affiliations,files=files,venues=artifact_venue)
