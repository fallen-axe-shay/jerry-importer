import sys
import six
import os
import logging
import dateutil.parser
import datetime
import urllib
import requests
from future.utils import raise_from
from urllib.parse import urlparse
import bs4
import re
import bibtexparser
import json

from searcch.importer.importer import BaseImporter
from searcch.importer.db.model import (
    Artifact, ArtifactFile, ArtifactMetadata, ArtifactRelease, User, Person,
    Importer, Affiliation, ArtifactAffiliation, ArtifactTag, Organization,
    Badge, ArtifactBadge, Venue, ArtifactVenue)

LOG = logging.getLogger(__name__)

class NDSSImporter(BaseImporter):
    """Provides an NDSS Importer."""

    name = "ndss"
    version = "0.1"

    @classmethod
    def _extract_record_id(self, url):
        try:
            session = requests.session()
            res = requests.get(url)
            urlobj = urlparse(res.url)
            if urlobj.netloc.endswith("ndss-symposium.org") \
              and urlobj.path.startswith("/ndss"):
                return (urlobj.path[len("/ndss-paper/"):].rstrip("/"), res.url, session)
        except BaseException:
            return None

    @classmethod
    def can_import(cls, url):
        """Checks to see if this URL is a doi.org or ndss-symposium.org/ndss-paper URL."""
        if cls._extract_record_id(url):
            return True
        return False

    def import_artifact(self, candidate):
        """Imports an artifact from NDSS and returns an Artifact, or throws an error."""
        url = candidate.url
        LOG.debug("importing '%s' from NDSS " % (url,))
        page = requests.get(url)
        soup = bs4.BeautifulSoup(page.content, 'html.parser')
        cat = soup.find('div',class_="page-header")
        if cat:
            title = cat.text
            auth = soup.find('p',class_="ndss_authors")
            if auth:
                auth = auth.text.replace("Author(s):" , "")
                auth = auth.split(',')
                affiliations = []
                for i in auth:
                        person = Person(name=i.strip())
                        affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=person,org=None),roles="Author"))
            else:
                affiliations = []
                LOG.warn("no author data")
            artifact_files = []
            try:
                ppfd = soup.find('p',class_="ndss_downloads")
                if ppfd:
                    paperurl = "https://www.ndss-symposium.org/"
                    url_route = ppfd.find('a')['href']
                    path = urlparse(url_route).path
                    ext = os.path.splitext(path)
                    typ = ext[1][1:]
                    name = ext[0].split('/')[-1]+ext[1]
                    paperurl = paperurl + url_route
                    artifact_files.append(ArtifactFile(
                                name=name, url=paperurl, filetype="application/"+typ))
                ppfd = soup.find('p',class_="ndss_additional")
                if ppfd:
                    paperurl = "https://www.ndss-symposium.org/"
                    url_route = ppfd.find('a')['href']
                    path = urlparse(url_route).path
                    ext = os.path.splitext(path)
                    typ = ext[1][1:]
                    pdfurl = paperurl + url_route
                    artifact_files.append(ArtifactFile(
                                name=name, url=pdfurl, filetype="application/"+typ))
            except:
                LOG.warning("failed to parse artifact files" ) 
                LOG.exception(sys.exc_info()[1])
            abstract = ""
            cont  = soup.find('div',class_='content')
            if cont:
                para = cont.find_all('p')
                if para:
                    abstract = para[-1].text
            metadata = []
            # Extract Venue Metadata
            venue_title = ""
            venue_url = None
            artifact_venue = []
            venue_verified = False
            venue_type = "conference"
            venue_object = None
            try:
                venue_link = soup.find('p',class_='ndss_associated')
                if venue_link:
                    venue_url = venue_link.find('a')['href']
                    venue_title = venue_link.find('a').text
                else:
                    venue_url = url
                venue_object = self.session.query(Venue).\
                filter(Venue.url  == venue_url).\
                filter(Venue.verified == True).first()
            except:
                LOG.warn("failed while getting venue object")
                LOG.exception(sys.exc_info()[1])  
            try:
                if venue_object:
                    artifact_venue.append(ArtifactVenue(venue = venue_object))
                    venue_verified = True
                value = json.dumps(dict(type=venue_type,title=venue_title ,url=venue_url,verified=venue_verified))
                metadata.append(ArtifactMetadata(
                        name="venue",value=value,type="text/json",
                        source="ndss"))
            except:
                LOG.warn("failed to scrape existing venue info")
                LOG.exception(sys.exc_info()[1])
        else:
            title = soup.find('h1',class_='entry-title')
            if not title:
                LOG.warn("no title in data")
            else:
                title = title.text

            cont  = soup.find('div',class_='entry-content')
            authors = []
            abstract = ""
            if cont:
                p = cont.find_all('p')
                if p:
                    if(len(p)>1):
                        authors = p[1].text.split(")")
                    if(len(p)>2):
                        abstract = p[2].text
            if len(abstract)==0:
                LOG.warn("no abstract in data")
            try:
                author_list = []
                affiliations = []
                org_dict = {}
                for i in authors:
                    if i:
                        aff = re.findall(r'\(.*?\)', i+")")
                        if aff:
                            aff = aff[0].strip()
                            aff = aff[1:-1]
                        auth = i.split("(")[0]
                        auth = auth.replace(","," ")
                        author_list.append(auth.strip())
                        offset = 1
                        org = None
                        if aff not in org_dict:
                            org = Organization(name=aff, type="Institution",
                                                country=None)
                            org_dict[aff] = org
                        else:
                            org = org_dict[aff]
                        person = Person(name=auth)
                        affiliations.append(ArtifactAffiliation(affiliation=Affiliation(person=person,org=org),roles="Author"))
            except:
                LOG.warning("failed to parse author list" ) 
                LOG.exception(sys.exc_info()[1])
            if not affiliations:
                    LOG.warn("no authors in metadata")

            metadata = list()

            presentation_det= soup.find_all('div', class_='btn-group-vertical')
            if len(presentation_det)>2:
                presentation_video_link = presentation_det[2].find('a')['href']
                parsed_url = urlparse(presentation_video_link)
                if not parsed_url.scheme:
                    presentation_video_link = "https:" + presentation_video_link
                metadata.append(ArtifactMetadata(
                            name="presentation_video", value=str(presentation_video_link), type="text/json",
                            source="ndss"))

            artifact_files = []
            if len(presentation_det)>0:
                presentation_pdf_link = presentation_det[0].find('a')['href']
                path = urlparse(presentation_pdf_link).path
                ext = os.path.splitext(path)
                typ = ext[1][1:]
                name = ext[0].split('/')[-1]+ext[1]
                artifact_files.append(ArtifactFile(
                            name=name, url=presentation_pdf_link, filetype="application/"+typ))
    
            if len(presentation_det)>0:
                presentation_slides_link = presentation_det[1].find('a')['href']
                path = urlparse(presentation_slides_link).path
                ext = os.path.splitext(path)
                typ = ext[1][1:]
                name = ext[0].split('/')[-1]+ext[1]
                artifact_files.append(ArtifactFile(
                            name=name, url=presentation_slides_link, filetype="application/"+typ))
            #Extract the venue information
            try:
                venue_url = url
                venue_title = ""
                venue_type = "conference"
                venue_verified = False
                artifact_venue = []
                if presentation_pdf_link:
                    year = presentation_pdf_link.split("/")[-3]
                    venue_url = "https://www.ndss-symposium.org/ndss" + year + "/"
                    venue_title = "NDSS Symposium "+year
                venue_object = self.session.query(Venue).\
                filter(Venue.url  == venue_url).\
                filter(Venue.verified == True).first()
            except:
                LOG.warn("failed while getting venue object")
                LOG.exception(sys.exc_info()[1])  
            try:
                if venue_object:
                    artifact_venue.append(ArtifactVenue(venue=venue_object))
                    venue_verified = True
                keys = ['title' , 'abbrev' , 'url' , 'description' , 'location' , 'year' , 'month' , 'start_day' ,'end_day' , 'publisher' , 'publisher_location' , 'publisher_url' , 'isbn' , 'issn' , 'doi' , 'volume' , 'issue' , 'verified']
                value = dict.fromkeys(keys)
                value['type'] = venue_type
                value['title'] = venue_title
                value['url'] = venue_url
                value['verified'] =venue_verified
                if len(artifact_venue) == 0:
                    artifact_venue.append(ArtifactVenue(venue = Venue(title = value['title'] , type = value['type'],abbrev = value['abbrev'], url = value['url'], description = value['description'], location = value['location'], year = value['year'], month = value['month'], start_day = value['start_day'],end_day = value['end_day'],publisher = value['publisher'], publisher_location = value['publisher_location'], publisher_url = value['publisher_url'], isbn = value['isbn'], issn = value['issn'],doi = value['doi'], volume = value['volume'],issue = value['issue'], verified = value['verified'])))
            except:
                LOG.warn("failed to scrape existing venue info")
                LOG.exception(sys.exc_info()[1])

        return Artifact(
            type="publication",url=url,title=title,description=abstract,
            name=title,ctime=datetime.datetime.now(),ext_id=url,
            owner=self.owner_object,importer=self.importer_object,
            tags=[],meta=metadata,files=artifact_files,affiliations=affiliations,venues=artifact_venue)
